import React, { useState, useEffect } from 'react';
import { Alert,Linking } from "react-native";
import SplashScreen from './Screens/SplashScreen';
import LoginScreen from "./Screens/Login";

// Import Navigators from React Navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DrawerNavigationRoutes from './Screens/DrawerNavigatorRoutes';
import { CommonApiCall } from "./Common/CommonApiCall";
import NetInfo from "@react-native-community/netinfo";
import { Utils } from './Common/Utils';
import OtherScreen from "./Screens/LeadsDetails";
import PurchaseDetailsScreen from "./Screens/PurchaseDetails";
import BuyNewCreditsScreen from "./Screens/BuyNewCredits";
const Stack = createStackNavigator();

const Auth = () => {
  // Stack Navigator for Login and Sign up Screen
  return (
    <Stack.Navigator initialRouteName="LoginScreen">
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};


const App = () => {
  const [permissions, setPermissions] = useState({});


  useEffect(() => {

    console.log("check ios version >>> "+ Utils.IosVersion)


    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type +"  "+ (Platform.OS == "ios") ? Utils.IosVersion : Utils.AndroidVerion);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        var data = new FormData();
        data.append('app_version', (Platform.OS == "ios") ? Utils.IosVersion : Utils.AndroidVerion);
        data.append('source', (Platform.OS == "ios") ? "2" : "1");


        CommonApiCall('Profile/forceUpdate', data).then(res => {


          if (res.code == 1) {

            if (res.data == 1) {
              Alert.alert(
                "The renewable Energy Hub",
                "A new version of Renewable is available. Please update your app",
                [

                  { text: "Update", onPress: () => Linking.openURL((Platform.OS == "ios") ? Utils.IosUrl : Utils.AndroidUrl) }
                ],
                { cancelable: false }
              );
            }

            console.log("check ForceUpdateCheck api data >>>>>> " + res.message)

          } else {
            // Alert.alert(res.message)
            console.log("check ForceUpdateCheck api data >>>>>> " + res.message)
          }

        }).catch(error => {
          console.log("get error:::: " + error.error)


        });
      } else {
        Alert("No internet connection...");
      }

    });

  });


  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SplashScreen">
        {/* SplashScreen which will come once for 5 Seconds */}
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          // Hiding header for Splash Screen
          options={{ headerShown: false }}
        />
        {/* Auth Navigator which includer Login Signup will come once */}
        <Stack.Screen
          name="Auth"
          component={Auth}
          options={{ headerShown: false }}
        />
        {/* Navigation Drawer as a landing page */}
        <Stack.Screen
          name="DrawerNavigationRoutes"
          component={DrawerNavigationRoutes}
          // Hiding header for Navigation Drawer as we will use our custom header
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Other"
          component={OtherScreen}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Purchasedetail"
          component={PurchaseDetailsScreen}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
        name="Buycreditscrn"
        component={BuyNewCreditsScreen}
        options={{
          headerShown: false
        }}

      />
      </Stack.Navigator>
    </NavigationContainer>
  );
};



export default App;
