import React, { Component, useRef } from 'react';
import axios from 'axios';
import { Utils } from '../Common/Utils';


export const TokenCommonApiCall =
    (url, data, token) =>
        new Promise((resolve, reject) => {
            console.log("get token  " + data)

            const headers = {
              
                'Content-Type': 'application/x-www-form-urlencoded',
                'token': token
            }

            // console.log("header data get  >>>> "+headers)


            return axios.post(Utils.BASE_URL + url, data, {
                headers: headers
            }).then(response => {
              //  console.log("check api resopne ", response)
                return resolve(response.data)
            })
                .catch(error => { return reject(error) });
        });


export const OnlyTokenCommonApiCall =
    (url, token) =>
        new Promise((resolve, reject) => {

             console.log("header data get  >>>> "+token)


            return axios.get(Utils.BASE_URL+url, { headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token': token
            } })
            .then(response => {
                // If request is good...
                console.log("check api resopne ", response)
                return resolve(response.data)
             })
            .catch((error) => {
                console.log("check OnlyTokenCommonApiCall Error response ", error.response.data)
                return reject(error)
             });


           
                
        });


export const CommonApiCall =
    (url, data, type) =>
        new Promise((resolve, reject) => {

            console.log("check login  "+  Utils.BASE_URL + url)

            return axios.post(Utils.BASE_URL + url, data, {
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(response => {
                console.log("check api resopne ", response)
                return resolve(response.data)
            })
                .catch(error => { return reject(error) });
        });