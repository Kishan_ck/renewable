import React, { Component, useState } from 'react';
import {color} from '../Common/Color';
import {TextInput} from 'react-native-paper';
import { View ,Image} from 'react-native';
export default ({ label, mode,type,value,onChangeText,secureTextEntry,flex,textContentType,dataDetectorTypes,keyboardType,left,iconpath,onPress,error,underlinecolor,primarycolor,maxLength,placeholder,editable,TextonPress}) => {
  
 // console.log("check ",onBlur)
  
    return (
       
               <TextInput
                    label={label}
                    mode={mode}
                    style={{ flex:flex,backgroundColor: color.color_white, fontSize: 16,marginTop:10 ,color: color.light_black,fontFamily:'Poppins-Light',borderBottomColor:color.color_white }}
                    selectionColor={color.dark_green}
                    placeholderTextColor={color.dark_green}
                    theme={{ colors: { placeholder: color.color_light_gray, text: color.color_black, primary: primarycolor,
                            background: color.color_white} }}
                    type={type}
                    value={value}
                    error={error}
                    onTouchStart={TextonPress}
                    onPress={TextonPress}
                    editable={editable}
                    onChangeText={onChangeText}
                    secureTextEntry={secureTextEntry}
                    textContentType={textContentType}
                    dataDetectorTypes={dataDetectorTypes}
                    keyboardType={keyboardType}
                    underlineColor={underlinecolor}
                    right={  <TextInput.Icon
                      name={() => (
                        <Image source={iconpath} />
                      )}
                      onPress={onPress}
                      forceTextInputFocus={false}
                      maxLength={maxLength}
                    /> }
                /> 
                 
                
      

    );
  };