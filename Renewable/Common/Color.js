export const color={
    color_white:"#ffffff",
    color_black:"#000",
    color_gray:"#333540",
    color_red:"#c60f07",
    color_light_gray:"#566C75",
    color_yellow:"#DCAC00",
    color_normal_gray:"#808899",
    color_light_blue:"#428bca",
    color_green:"#008000",
    color_off_white:"#DCDCDC",
    color_orange:"#fa7601",
}