import React, { Component } from 'react';
import { Text } from 'react-native';
import {actuatedNormalize} from '../Common/FontSize';

export default({numberOfLines,text,fontFamily,fontSize,textstyles,color,onPress})=>{
  //  console.log("check style ",textstyles);
    return(
        <Text   style={[textstyles,{fontFamily:fontFamily,color:color,fontSize:fontSize,margin:0,left:0}]}
        onPress={onPress}
        > {text}</Text>
    );
};

