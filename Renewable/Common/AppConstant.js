export const AppConstant ={

    EMAIL :'email',
    PASSWORD :'password',
    ID:'id',
    API_TOKEN:'api_token',
    NAME:'name',
    LEADSCREDITS:'leads_credits',
    FCM_TOKEN:'fcm_token',
}

export const ID = 'id'