export const Utils ={

    //BASE_URL:"https://stagingci.renewableenergyhub.co.uk/Api/",
    BASE_URL:"https://www.renewableenergyhub.co.uk/Api/",
    BASE_URL_PAYMENT:"https://www.renewableenergyhub.co.uk/api/",
    NAME:'',
    FCMTOKEN:'',
    IsAPICall:false,
    AndroidVerion:'1.6',
    IosVersion:'1.8',
    AndroidUrl:"https://play.google.com/store/apps/details?id=com.coderkube.renewable",
    IosUrl:"https://apps.apple.com/us/app/renewable-energy-hub/id1559013094",

}

