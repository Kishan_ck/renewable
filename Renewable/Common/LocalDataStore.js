
import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeData = async (key, data) => {
    try {
      await AsyncStorage.setItem(key, data);
    } catch (error) {
      throw new Error(error);
    }
  };

  export const removeData = async (key) => {
    try {
      await AsyncStorage.removeItem(key);
    } catch (error) {
      throw new Error(error);
    }
  };
  export const retrieveData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key);
      return value;
    } catch (error) {
      return null;
    }
  };

  export const removeAllData = () => {
    try {
       AsyncStorage.clear();
    } catch (error) {
      throw new Error(error);
    }
  };

  export const NoretrieveData = async (key) => {
    try {
      const value =  AsyncStorage.getItem(key);
      return value;
    } catch (error) {
      return null;
    }
  };


  export const storeJsonData = async (key,value) => {
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem(key, jsonValue)
    } catch (e) {
      // saving error
    }
  }


  export const getJsonData = async (key) => {
    try {
      const jsonValue = await AsyncStorage.getItem(key)
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      // error reading value
    }
  }