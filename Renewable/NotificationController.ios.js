import React, { useEffect } from 'react';
import { Alert } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import PushNotificationIos from '@react-native-community/push-notification-ios';

const NotificationControllerIos = (props) => {
  PushNotification.configure({
    onNotification: (notification) => {
      if (notification) {
        console.log("NotificationControllerIos"+ notification);
        //Alert.alert('Opened push notification', JSON.stringify(notification));
       
      }
    },
  });

useEffect(() => {
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      PushNotificationIos.addNotificationRequest({
        id: remoteMessage.messageId,
        body: remoteMessage.notification.body,
        title: remoteMessage.notification.title,
        userInfo: remoteMessage.data,
        data:remoteMessage,
      });
    });

    console.log("check notification come")

    PushNotificationIos.getApplicationIconBadgeNumber((num)=>{ // get current number
     
        PushNotificationIos.setApplicationIconBadgeNumber(num+1) //set number to 0
    
  });
    return unsubscribe;
  }, []);

  return null;
};

export default NotificationControllerIos;