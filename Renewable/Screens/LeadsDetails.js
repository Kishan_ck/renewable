import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View, Platform, Image, TouchableOpacity, ActivityIndicator, Alert, BackHandler, useWindow, Dimensions, Text, FlatList, ImageBackground, NativeModules, Modal } from 'react-native';
import { color } from '../Common/Color';
import { Appbar, Badge, Card } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import { storeData, retrieveData } from '../Common/LocalDataStore';
import { AppConstant } from '../Common/AppConstant';
import Spinner from 'react-native-loading-spinner-overlay';
import NetInfo from "@react-native-community/netinfo";
import { CommonApiCall, TokenCommonApiCall, OnlyTokenCommonApiCall } from "../Common/CommonApiCall";
import { WebView } from "react-native-webview";
import { Utils } from '../Common/Utils';
// let MFLReactNativePayPal = NativeModules.MFLReactNativePayPal;

// MFLReactNativePayPal.initializePaypalEnvironment("0", "AYdaFJcXcO1KdJ8J2BcjdE6X66I9Mch9KpuGYHssFOaCEc5QmVNOj1Y4K_N0sevMoDteYXm0dhh8rkaQ");


class LeadsDetails extends Component {


    webview = null;
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            token: "",
            isLoading: false,
            data: {},
            Alert_Visibility: false,
            isWeb: false,
            windowTitle: "",
            url: "",
            leadcredit: ""
        };
    }


    paypal() {
        this.setState({ Alert_Visibility: false });
    }

    getPaypalData(confrim) {
        console.log("get payment id >>>>> " + confrim.response.id)
        Alert.alert("Your payment is successful.")
    }

    componentDidMount() {

        console.log("check Render time")

        // if (this.state.isWeb) {
        BackHandler.addEventListener('hardwareBackPress', () => { return true });
        // }

        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);

            if (state.isConnected) {

                  this.getData();
                this.AvilabeLeads();
            } else {
                Alert("No internet connection...");
            }

        });

    }


    async WorldPayPayment() {

        this.setState({ Alert_Visibility: false, isWeb: true, url: `${Utils.BASE_URL_PAYMENT}wordpay-checkout/${this.state.data.id}/${this.state.id}` })

        console.log("check Word pay >>>>  " + `${Utils.BASE_URL_PAYMENT}wordpay-checkout/${this.state.data.id}/${this.state.id}`)

    }

    async PaypalPayment() {

        this.setState({ Alert_Visibility: false, isWeb: true, url: `${Utils.BASE_URL_PAYMENT}payment-checkout/${this.state.data.id}/${this.state.id}` })

        console.log("check Word pay >>>>  " + `${Utils.BASE_URL_PAYMENT}payment-checkout/${this.state.data.id}/${this.state.id}`)

    }

    async AvilabeLeads() {


        this.setState({ token: await retrieveData(AppConstant.API_TOKEN), })

        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
            this.setState({
                isLoading: true,

            })

            if (state.isConnected) {




                OnlyTokenCommonApiCall("Profile/avilabe_leads", this.state.token).then(res => {


                    if (res.code == 1) {


                        console.log("check data >>>>>  " + res.data);

                        this.setState({
                            isLoading: false,
                            leadcredit: res.data.leads_credits,

                        })

                    } else {
                        this.setState({ isLoading: false })
                        Alert.alert(res.message)

                    }

                }).catch(error => {
                    console.log("get error:::: " + error.error)
                    this.setState({ isLoading: false })

                });

            } else {
                Alert("No internet connection...");
            }

        })



    }



    async BuyLeadsUsingCredit() {




        Alert.alert(
            "The renewable Energy Hub",
            "Are you sure you want to buy a lead using credits.",
            [

                {
                    text: "OK", onPress: () =>

                        NetInfo.fetch().then(state => {
                            console.log("Connection type", state.type);
                            console.log("Is connected?", state.isConnected);
                            this.setState({
                                isLoading: true,
                                Alert_Visibility: false
                            })

                            if (state.isConnected) {
                                var data = new FormData();
                                //data.append('amount', this.state.data.price);
                                //data.append('leadId', this.state.data.id);
                                data.append('lead', this.state.data.id)
                                data.append('installer', this.state.id)

                                TokenCommonApiCall("Profile/payment_with_credit", data, this.state.token).then(res => {


                                    if (res.code == 1) {


                                        TokenCommonApiCall("Profile/leadPurchase/" + res.data.payment.payment_session, data, this.state.token).then(res => {

                                            console.log("check data >>>>>  " + res.data);

                                            this.setState({
                                                isLoading: false,
                                            })

                                            this.props.navigation.navigate('Purchasedetail', { lead_id: this.state.data.id })

                                        }).catch(error => {
                                            console.log("get error:::: " + error.error)
                                            this.setState({ isLoading: false })

                                        });



                                    } else {
                                        this.setState({ isLoading: false })
                                        Alert.alert(res.message)

                                    }

                                }).catch(error => {
                                    console.log("get error:::: " + error.error)
                                    this.setState({ isLoading: false })

                                });

                            } else {
                                Alert("No internet connection...");
                            }

                        })

                },
                {
                    text: "Cancel",
                    onPress: () => this.setState({ Alert_Visibility: false }),
                    style: "cancel"
                },
            ],
            { cancelable: false }

        );





    }


    async getData() {
        const leadid = this.props.route.params.lead_id;
        console.log("get leadid>>>>>>> " + leadid)
        this.setState({

            token: await retrieveData(AppConstant.API_TOKEN),
            id: await retrieveData(AppConstant.ID),
            isLoading: true,

        })



        var data = new FormData();
        data.append('lead_id', leadid)


        TokenCommonApiCall("Profile/lead_data", data, this.state.token).then(res => {


            if (res.code == 1) {

                console.log("check lead data object >>>>>>  ", res)
                this.setState({
                    data: res.data,
                    isLoading: false,

                })

            } else {
                this.setState({ isLoading: false })
                Alert.alert(res.message)

            }

        }).catch(error => {
            console.log("get error:::: " + error.error)
            this.setState({ isLoading: false })

        });



        console.log("get id >>>>> " + this.state.data.id)



    }


    handleWebViewNavigationStateChange = newNavState => {
        // newNavState looks something like this:
        // {
        //   url?: string;
        //   title?: string;
        //   loading?: boolean;
        //   canGoBack?: boolean;
        //   canGoForward?: boolean;
        // }
        const { url } = newNavState;


        if (url.includes('paid')) {
            this.webview.stopLoading();
            Alert.alert("Payment successfully")
            this.setState({ isWeb: false })
        }

        if (url.includes('dispatcher')) {
            this.setState({ isWeb: false })
        }

        if (url.includes('paypal/cancel_return')) {
            this.setState({ isWeb: false })
        }


    };





    render() {


        const SCRIPT = `
        const meta = document.createElement('meta');
        meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
        meta.setAttribute('name', 'viewport');
        document.head.appendChild(meta);`;


        return (
            <View style={styles.container}>

                <Spinner
                    visible={this.state.isLoading}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle} />

                <View style={{ flex: 1, height: '100%', display: (this.state.isWeb == true ? 'flex' : 'none') }}>

                    <SafeAreaView style={{ flex: 1, height: "100%", width: '100%', resizeMode: 'cover' }}>

                        <WebView
                            ref={ref => (this.webview = ref)}
                            source={{ uri: this.state.url }}
                            javaScriptCanOpenWindowsAutomatically={true}
                            setSupportMultipleWindows={true}
                            domStorageEnabled={true}
                            decelerationRate="normal"
                            startInLoadingState={true}
                            automaticallyAdjustContentInsets={true}
                            androidHardwareAccelerationDisabled={true}
                            style={{ height: "100%", width: '100%', resizeMode: 'cover', flex: 1 }}
                            scalesPageToFit={true}
                            injectedJavaScript={SCRIPT}
                            onNavigationStateChange={this.handleWebViewNavigationStateChange} />

                    </SafeAreaView>


                </View>


                <View style={{ flex: 1, display: (this.state.isWeb == true ? 'none' : 'flex') }}>


                    <Appbar.Header style={{ backgroundColor: color.color_light_gray }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                            <Image source={require('../assets/back.png')} style={{ height: 20, width: 20, marginLeft: 15, }}></Image>
                        </TouchableOpacity>

                        <Appbar.Content title="Job Details" titleStyle={{ color: color.color_white, fontFamily: 'Poppins-SemiBold' }} />
                    </Appbar.Header>
                    <LinearGradient
                        colors={['#566C75', '#66859C', '#333540']}
                        style={[styles.container]}>
                        <ScrollView>

                            <SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>


                                <View style={styles.cardview}>


                                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', }}>

                                        <View style={{ flexDirection: 'row' }}>

                                            <View style={{ flex: 1, flexDirection: 'column' }}>

                                                <Text
                                                    style={{ fontFamily: "Poppins-Bold", fontSize: 18, color: color.color_black }}>Client Type</Text>
                                                <Text
                                                    style={{ fontFamily: "Poppins-Light", fontSize: 18, color: color.color_black }}>{this.state.data.property_type}</Text>

                                            </View>


                                            <View style={{ width: 1, height: 30, backgroundColor: color.color_gray, justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }} />

                                            <View style={{ flex: 1, }}>


                                                <Text
                                                    style={{ fontFamily: "Poppins-Bold", fontSize: 18, color: color.color_black, alignSelf: 'flex-end' }}>Code</Text>
                                                <Text
                                                    style={{ fontFamily: "Poppins-Light", fontSize: 18, color: color.color_black, alignSelf: 'flex-end' }}>{this.state.data.unique}</Text>


                                                {/* <CommonText
                                text={"Code"}
                                fontFamily="Poppins-Bold"
                                fontSize={16}
                                color={color.color_gray}
                                textstyles={{alignSelf:'flex-end'}}/>

                            <CommonText
                                text={item.code}
                                fontFamily="Poppins-Light"
                                fontSize={16}
                                color={color.color_black}
                                textstyles={{alignSelf:'flex-end'}}/> */}



                                            </View>



                                        </View>

                                        <View style={{ width: '100%', height: 1, backgroundColor: color.color_gray, marginTop: 15, }} />


                                        <View style={{ marginTop: 10, }}>

                                            {/* 
                            <Text
                                style={{fontFamily:"Poppins-Bold",fontSize:14,color:color.color_black}}>Lead Option</Text>
                            <Text
                                style={{fontFamily:"Poppins-Light",fontSize:14,color:color.color_black}}>Lead Option</Text> */}


                                            {/* <CommonText
                                    text="Lead Option"
                                    fontFamily="Poppins-Bold"
                                    fontSize={14}
                                    color={color.color_black}/>

                                <CommonText
                                    text={item.leadoption}
                                    fontFamily="Poppins-Light"
                                    fontSize={14}
                                    color={color.color_black}
                                /> */}



                                        </View>

                                        <View style={{ marginTop: 10, }}>


                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Location</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.city}</Text>

                                        </View>

                                        <View style={{ marginTop: 10, }}>


                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Install Type </Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.type}</Text>

                                        </View>

                                        <View style={{ marginTop: 10 }}>


                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Date Received  </Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>Date Received  </Text>

                                        </View>

                                        <View style={{ marginTop: 10 }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Project Details</Text>

                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.message}</Text>

                                        </View>

                                        <View style={{ marginTop: 10, flexDirection: 'column' }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Price</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.price_credits + " Credits"}</Text>



                                        </View>

                                        <View style={{ marginTop: 10, flexDirection: 'column' }}>


                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Distance</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.distance}</Text>
                                        </View>

                                        <View style={{ marginTop: 10, flex: 1, flexDirection: 'column' }}>

                                            <View style={{ width: '100%' }}>
                                                <TouchableOpacity onPress={() => {

                                                    if (this.state.leadcredit > 0) {
                                                        this.BuyLeadsUsingCredit()

                                                    } else {

                                                        Alert.alert(
                                                            "The renewable Energy Hub",
                                                            "Click Ok to buy credits.",
                                                            [

                                                                {
                                                                    text: "OK", onPress: () =>

                                                                        this.props.navigation.navigate('settingScreenStack')

                                                                },
                                                                {
                                                                    text: "Cancel",
                                                                    style: "cancel"
                                                                },
                                                            ],
                                                            { cancelable: false }

                                                        );



                                                    }


                                                }}>
                                                    <Card style={{ elevation: 10, borderRadius: 10, backgroundColor: color.color_red, width: '100%' }}>

                                                        <Text
                                                            style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_white, padding: 10, alignSelf: 'center', justifyContent: 'center' }}>{"Buy Lead Credit (" + this.state.leadcredit + " credits avaiable)"}</Text>

                                                    </Card>

                                                </TouchableOpacity>


                                            </View>
                                        </View>

                                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", alignSelf: 'center' }}>

                                            <Modal
                                                visible={this.state.Alert_Visibility}
                                                transparent={true}
                                                animationType={"fade"}
                                                onRequestClose={() => { this.cancelAlertBox(!this.state.Alert_Visibility) }} >

                                                <View style={{
                                                    flex: 1,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                    backgroundColor: "rgba(0, 0, 0, 0.2)", justifyContent: "center", alignItems: "center",
                                                }}>

                                                    <View style={{ width: '70%', backgroundColor: color.color_white, padding: 20, borderRadius: 10, }}>

                                                        {/* <Text
                                                            onPress={() => this.PaypalPayment()}
                                                            style={{ fontFamily: "Poppins-Bold", fontSize: 14, color: color.color_white, padding: 10, alignSelf: 'center', backgroundColor: color.color_light_gray, width: '100%', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>Pay with PayPal</Text> */}

                                                        <Text
                                                            onPress={() => {


                                                            }}
                                                            style={{ marginTop: 10, fontFamily: "Poppins-Bold", fontSize: 14, color: color.color_white, padding: 10, alignSelf: 'center', backgroundColor: color.color_light_gray, width: '100%', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>Use {this.state.data.price_credits} {"Credit (" + this.state.leadcredit + " credits avaiable) 1 credit = £5"}</Text>

                                                        {/* <Text
                                                            onPress={() => this.WorldPayPayment()}
                                                            style={{ marginTop: 10, fontFamily: "Poppins-Bold", fontSize: 14, color: color.color_white, padding: 10, alignSelf: 'center', backgroundColor: color.color_light_gray, width: '100%', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>Pay with Credit / Debit Card</Text> */}

                                                        <Text
                                                            onPress={() => this.setState({ Alert_Visibility: false, })}
                                                            style={{ marginTop: 10, fontFamily: "Poppins-Bold", fontSize: 14, color: color.color_white, padding: 10, alignSelf: 'center', backgroundColor: color.color_red, width: '100%', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>Cancel</Text>


                                                    </View>

                                                </View>

                                            </Modal>

                                        </View>
                                    </View>


                                </View>



                            </SafeAreaView>

                        </ScrollView>
                    </LinearGradient>

                </View>


            </View>



        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    cardview: {
        flex: 1,
        backgroundColor: color.color_white,
        borderRadius: 10,
        elevation: 15,
        margin: 15,
        padding: 10,
    },
    MainAlertView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#1769aa",
        height: 200,
        width: '90%',
        borderColor: '#fff',
    },
    AlertTitle: {
        fontSize: 25,
        color: "#fff",
        textAlign: 'center',
        padding: 10,
        height: '28%'
    },
    AlertMessage: {
        fontSize: 22,
        color: "#fff",
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 10,
        height: '40%'
    },
    buttonStyle: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    TextStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 22,
        marginTop: -5
    }

})
export default LeadsDetails;
