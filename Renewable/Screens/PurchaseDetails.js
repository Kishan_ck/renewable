import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View, Platform, Image, TouchableOpacity, ActivityIndicator, Alert, BackHandler, useWindow, Dimensions, Text, FlatList, ImageBackground, Linking, TouchableWithoutFeedback } from 'react-native';
import { color } from '../Common/Color';
import { Appbar, Badge, Card } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import { storeData, retrieveData } from '../Common/LocalDataStore';
import { AppConstant } from '../Common/AppConstant';
import Spinner from 'react-native-loading-spinner-overlay';
import NetInfo from "@react-native-community/netinfo";
import { CommonApiCall, TokenCommonApiCall } from "../Common/CommonApiCall";


class PurchaseDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            token: "",
            isLoading: false,
            data: {},

        };
    }


    componentDidMount() {

        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);

            if (state.isConnected) {

                this.getData();
            } else {
                Alert("No internet connection...");
            }

        });



    }


    async getData() {
        const leadid = this.props.route.params.lead_id;


        this.setState({
            id: leadid,
            token: await retrieveData(AppConstant.API_TOKEN),
            isLoading: true
        })


        var data = new FormData();
        data.append('lead_id', this.state.id);


        TokenCommonApiCall("Profile/lead_data", data, this.state.token).then(res => {


            if (res.code == 1) {

                this.setState({
                    isLoading: false,
                    data: res.data
                })

                console.log("check data in state data >>>>>>  " + this.state.data.id);

            } else {
                this.setState({ isLoading: false })
                Alert.alert(res.message)

            }

        }).catch(error => {
            console.log("get error:::: " + error.error)
            this.setState({ isLoading: false })

        });


    }


    render() {
        return (
            <View style={styles.container}>

                <Spinner
                    visible={this.state.isLoading}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle} />

                <Appbar.Header style={{ backgroundColor: color.color_light_gray }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("HomeScreen")} >
                        <Image source={require('../assets/back.png')} style={{ height: 20, width: 20, marginLeft: 15, }}></Image>
                    </TouchableOpacity>

                    <Appbar.Content title="Client Details" titleStyle={{ color: color.color_white, fontFamily: 'Poppins-SemiBold' }} />
                </Appbar.Header>
                <LinearGradient
                    colors={['#566C75', '#66859C', '#333540']}
                    style={styles.container}>

                    <ScrollView>

                        <SafeAreaView style={{ flex: 1, flexDirection: 'column', }}>

                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <View style={styles.cardview}>

                                    <View style={{ flex: 1, flexDirection: 'column' }}>

                                        <View style={{ flexDirection: 'row' }}>

                                            <View style={{ flex: 1, flexDirection: 'column' }}>

                                                <Text
                                                    style={{ fontFamily: "Poppins-Bold", fontSize: 18, color: color.color_black }}>Type Of Property</Text>
                                                <Text
                                                    style={{ fontFamily: "Poppins-Light", fontSize: 18, color: color.color_black }}>{this.state.data.property_type}</Text>

                                            </View>


                                            <View style={{ width: 1, height: 30, backgroundColor: color.color_gray, justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }} />

                                            <View style={{ flex: 1, marginLeft: 10, }}>
                                                <Text
                                                    style={{ fontFamily: "Poppins-Bold", fontSize: 18, color: color.color_black, alignSelf: 'flex-end' }}>Quotation Code</Text>
                                                <Text
                                                    style={{ fontFamily: "Poppins-Light", fontSize: 18, color: color.color_black, alignSelf: 'flex-end' }}>{this.state.data.unique}</Text>
                                            </View>



                                        </View>

                                        <View style={{ width: '100%', height: 1, backgroundColor: color.color_gray, marginTop: 15, }} />


                                        <View style={{ marginTop: 10, }}>
                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Category</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.category}</Text>

                                        </View>

                                        <View style={{ marginTop: 10, }}>


                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Name</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.name}</Text>


                                        </View>

                                        <View style={{ marginTop: 10 }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Email Address</Text>
                                            <Text
                                                onPress={() => {
                                                    if (this.state.data.email != "") {
                                                        Linking.openURL('mailto:' + this.state.data.email)
                                                    }
                                                }}
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.email}</Text>

                                        </View>

                                        <View style={{ marginTop: 10 }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Phone Number</Text>


                                            <TouchableWithoutFeedback onPress={() => Linking.openURL(`tel:${this.state.data.phone}`)}>
                                                <Text
                                                    style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_green }}>{this.state.data.phone}</Text>
                                            </TouchableWithoutFeedback>

                                            {/* <ReadMore numberOfLines={3} style={{fontSize:16,fontFamily:'Poppins-Light',}}>
                            {
                            "Client is a building developer"
                            }
                            </ReadMore>

                     */}

                                        </View>

                                        <View style={{ marginTop: 10, }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Mobile Number</Text>

                                            <TouchableWithoutFeedback onPress={() => Linking.openURL(`tel:${this.state.data.phone}`)}>
                                                <Text
                                                    style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_green }}>{this.state.data.mobile}</Text>
                                            </TouchableWithoutFeedback>
                                        </View>

                                        <View style={{ marginTop: 10, }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Address</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.address}</Text>

                                        </View>

                                        <View style={{ marginTop: 10, }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Postcode</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.postcode}</Text>

                                        </View>

                                        <View style={{ marginTop: 10, }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>City</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.city}</Text>

                                        </View>

                                        <View style={{ marginTop: 10, }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Job Details</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.message}</Text>

                                        </View>

                                        {/* <View style={{marginTop:10,}}>

                <Text
                    style={{fontFamily:"Poppins-Bold",fontSize:16,color:color.color_black}}>Special Requirements</Text>
                <Text
                    style={{fontFamily:"Poppins-Light",fontSize:16,color:color.color_black}}>{this.state.data.message}</Text>

            </View> */}


                                        <View style={{ marginTop: 10, }}>

                                            <Text
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_black }}>Install Type</Text>
                                            <Text
                                                style={{ fontFamily: "Poppins-Light", fontSize: 16, color: color.color_black }}>{this.state.data.type}</Text>

                                        </View>




                                    </View>
                                </View>

                            </View>

                        </SafeAreaView>


                    </ScrollView>



                </LinearGradient>
            </View>




        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    cardview: {

        backgroundColor: color.color_white,
        borderRadius: 10,
        elevation: 15,
        margin: 15,
        padding: 10,

    }
})


export default PurchaseDetails;