import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View, FlatList, RefreshControl, Image, TouchableWithoutFeedback, ActivityIndicator, Alert, BackHandler, useWindow, Dimensions, ImageBackground, Linking, TouchableOpacity, Platform } from 'react-native';
import { color } from '../Common/Color';
import { AppConstant } from '../Common/AppConstant';
import { storeData, retrieveData, removeAllData } from '../Common/LocalDataStore';
import NetInfo from "@react-native-community/netinfo";
import { CommonApiCall, TokenCommonApiCall } from "../Common/CommonApiCall";
import { Appbar, Badge, Card } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import { Utils } from '../Common/Utils';
import messaging from '@react-native-firebase/messaging';
const { height } = Dimensions.get('window');
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from "react-native-push-notification";

class NewHome extends Component {

    constructor(props) {
        super(props)

        this.buypage = 1;
        this.handleBackButton = this.handleBackButton.bind(this)
        this.purchasepage = 1;
        this.state = {
            id: "",
            token: "",
            isBuyAllpage: false,
            isMainLoading: true,
            isLoading: false,
            isLoadingPurchase: false,
            name: "",
            buycolor: color.color_white,
            purchasecolor: color.color_off_white,
            IsBuy: true,
            buydata: [],
            purchasedata: [],
            leadcredit: '',
            count: 0, isOnline: null,
            isLoggedIn: 0,
            IsBuyNoData: false,
            IsPurchaseData: false,
            isFetching: false,
            isFetching_no: false,
            isFetchingPurchase: false,

        }
    }



    _onFocus = () => {
        console.log("=====_onFocus  " + Utils.IsAPICall)
        this.buypage = 1;
        this.handleBackButton = this.handleBackButton.bind(this)
        this.purchasepage = 1;
        this.setState({

            id: "",
            token: "",
            isBuyAllpage: false,
            isMainLoading: true,
            isLoading: false,
            isLoadingPurchase: false,
            name: "",
            buycolor: color.color_white,
            purchasecolor: color.color_off_white,
            IsBuy: true,
            buydata: [],
            purchasedata: [],
            leadcredit: '',
            leadcredit1: '',
            count: 0, isOnline: null,
            isLoggedIn: 0,
            IsBuyNoData: false,
            IsPurchaseData: false,
            isFetching: false,
            isFetchingPurchase: false,
        });
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        BackHandler.addEventListener('hardwareBackPress', () => { return false });

        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);

            if (state.isConnected) {
                this.getData()
            } else {
                Alert("No internet connection...");
            }

        });

        if (Platform.OS == "ios") {
            PushNotificationIos.getApplicationIconBadgeNumber((num) => { // get current number

                PushNotificationIos.setApplicationIconBadgeNumber(0) //set number to 0

            });

        }

        saySomething = (ids) => {
            //this.props.navigation.replace('DrawerNavigationRoutes');



            if (this.state.id != null) {
                this.props.navigation.navigate('Other', { lead_id: ids })
            } else {
                this.props.navigation.navigate('Auth');
            }


        };


        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: function (token) {
                console.log("TOKEN:", token);
            },

            // (required) Called when a remote is received or opened, or local notification is opened
            onNotification: function (notification) {
                console.log("NOTIFICATION:111", notification);

                if (Platform.OS == "ios") {
                    PushNotificationIos.getApplicationIconBadgeNumber((num) => { // get current number

                        PushNotificationIos.setApplicationIconBadgeNumber(0) //set number to 0

                    });

                }



                if (notification.foreground) {
                    if (Platform.OS == "ios") {
                        PushNotificationIos.getApplicationIconBadgeNumber((num) => { // get current number
    
                            PushNotificationIos.setApplicationIconBadgeNumber(0) //set number to 0
    
                        });
    
                    }
                    saySomething(notification.data.data.id);
                } else {
                    saySomething(notification.data.id);
                }


                // process the notification

                // (required) Called when a remote is received or opened, or local notification is opened
                notification.finish(PushNotificationIOS.FetchResult.NoData);
            },

            // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
            onAction: function (notification) {
                console.log("ACTION:", notification.action);
                console.log("NOTIFICATION:", notification);

                // process the action
            },

            // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
            onRegistrationError: function (err) {
                console.error(err.message, err);
            },

            // IOS ONLY (optional): default: all - Permissions to register.
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            // Should the initial notification be popped automatically
            // default: true
            popInitialNotification: true,

            /**
             * (optional) default: true
             * - Specified if permissions (ios) and token (android and ios) will requested or not,
             * - if not, you must call PushNotificationsHandler.requestPermissions() later
             * - if you are not using remote notification or do not have Firebase installed, use this:
             *     requestPermissions: Platform.OS === 'ios'
             */
            requestPermissions: true,
        });

    };

    _onBlur = () => {
        this.setState({ isFocused: false });
        console.log("=====_onBlur")
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    };


    onButtonPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        // then navigate
        navigate('NewScreen');
    }

    handleBackButton = () => {
        if (this.props.navigation.isFocused()) {
            console.log("this is newHome")
            Alert.alert(
                'Exit App',
                'Exiting the application?', [{
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                }, {
                    text: 'OK',
                    onPress: () => BackHandler.exitApp()
                },], {
                cancelable: false
            }
            )

            return true;
        } else {
            console.log("this is not newHome")
        }
    }


    async getData() {

        this.setState({
            name: await retrieveData(AppConstant.NAME),
        })


        // this.setState({
        //     leadcredit1: await retrieveData(AppConstant.LEADSCREDITS),
        // })

        this.getDataBuy(this.buypage)
        this.getDataPurchase(this.purchasepage)




    }



    onRefreshPurchas() {



        this.purchasepage = 1;
        console.log("onRefresh check buypage ?>>>> " + this.buypage)
        this.setState({ isLoadingPurchase: false, isFetchingPurchase: true, purchasedata: [], isMainLoading: false }, () => { this.getDataPurchase(1); });
    }

    onRefresh() {
        this.buypage = 1;
        console.log("onRefresh check buypage onRefresh ?>>>> " + this.buypage)
        this.setState({ isLoading: false, IsBuyNoData: false, isFetching: true, buydata: [], isBuyAllpage: true, isMainLoading: false }, () => { this.getDataBuy(this.buypage); });
    }


    onRefresh_no() {
        this.buypage = 1;
        console.log("onRefresh check buypage onRefresh_no ?>>>> " + this.buypage)
        this.setState({ isLoading: false, isFetching_no: true, buydata: [], isBuyAllpage: true, isMainLoading: false, IsBuyNoData: false }, () => { this.getDataBuy(this.buypage); });
    }

    async getDataBuy(page) {


        this.state.id = await retrieveData(AppConstant.ID)
        //this.state.token = await retrieveData(AppConstant.API_TOKEN)

        this.setState({
            token: await retrieveData(AppConstant.API_TOKEN),
            buydata: []
        })


        console.log("check page count ??????  " + page)

        console.log("check current token >>>>>>  " + this.state.token)

        var data = new FormData();
        data.append('per_page', "15");
        data.append('page', page);

        TokenCommonApiCall("Profile/leads_available", data, this.state.token).then(res => {

            console.log("get leads_credits INNN  data.credits.leads_credits???>>>>> " + res.code)

            // this.setState({ leadcredit: res.data.credits.leads_credits})

            if (res.code == 1) {

                this.setState({ buydata: [] })

                let listData = this.state.buydata;
                let data = listData.concat(res.data.installer)
                this.setState({ leadcredit: res.data.credits.leads_credits, isFetching_no: false, isFetching: false, buydata: data, isMainLoading: false, isBuyAllpage: true, IsBuyNoData: false })
                console.log("get data ???>>>>> " + page)
                console.log("listData.length >>>>>> " + data.length)
                if (data.length >= 15) {
                    this.setState({ isLoading: true })
                }
            } else if (res.code == 0) {
                console.log("no data in " + this.state.IsBuyNoData)
                this.setState({ isFetching_no: false, isFetching: false, leadcredit: res.data.leads_credits, isLoading: false, isBuyAllpage: true, isMainLoading: false })
                this.setState({ IsBuyNoData: true })

            } else {

                Alert.alert(
                    "The renewable Energy Hub",
                    res.message,
                    [

                        {
                            text: "Ok", onPress: () => {
                                removeAllData();
                                this.props.navigation.replace('Auth');
                            }
                        }
                    ],
                    { cancelable: false }
                );
                this.setState({ isFetching_no: false, isFetching: false, isLoading: false, isBuyAllpage: false, isMainLoading: false })

            }

        }).catch((error) => {

            // this.setState({
            //     leadcredit: this.state.leadcredit1 ,
            // })
            console.log("get leads_credits get error:::: " + error)
            this.setState({ isFetching: false, isLoading: false, isBuyAllpage: true, isMainLoading: false })

        });



    }


    async getDataBuyLoadMore(page) {


        this.state.id = await retrieveData(AppConstant.ID)
        //this.state.token = await retrieveData(AppConstant.API_TOKEN)

        this.setState({
            token: await retrieveData(AppConstant.API_TOKEN),

        })


        console.log("loadmore page  ???  " + page)

        var data = new FormData();
        data.append('per_page', "15");
        data.append('page', page);

        TokenCommonApiCall("Profile/leads_available", data, this.state.token).then(res => {

            if (res.code == "1") {

                let listData = this.state.buydata;
                let data = listData.concat(res.data.installer)
                this.setState({ isFetching: false, isLoading: true, buydata: data, isMainLoading: false, isBuyAllpage: true, leadcredit: res.data.credits.leads_credits, IsBuyNoData: false })
                console.log("get data ???>>>>> " + page)


            } else {
                this.setState({ isFetching: false, leadcredit: res.data.leads_credits, isLoading: false, isBuyAllpage: true, isMainLoading: false })
            }

        }).catch(error => {
            console.log("get leads_credits get error load more:::: " + error)
            this.setState({ isLoading: false, isBuyAllpage: true, isMainLoading: false })

        });



    }


    async getDataPurchase(page) {

        this.setState({
            name: await retrieveData(AppConstant.NAME),
        })

        this.state.id = await retrieveData(AppConstant.ID)
        //this.state.token = await retrieveData(AppConstant.API_TOKEN)

        this.setState({
            token: await retrieveData(AppConstant.API_TOKEN),
        })



        var data = new FormData();
        data.append('per_page', "15");
        data.append('page', page);



        TokenCommonApiCall("Profile/leads_taken", data, this.state.token).then(res => {

            if (res.code == 1) {
                let listData = this.state.purchasedata;
                let data = listData.concat(res.data)
                this.setState({ purchasedata: data, IsPurchaseData: false, isFetchingPurchase: false })
                // console.log("get data ???>>>>> "+this.state.data)    

                if (data.length >= 15) {
                    this.setState({ isLoadingPurchase: true, })
                }
            } else if (res.code == 0) {
                this.setState({ isLoadingPurchase: false, IsPurchaseData: true, isFetchingPurchase: false })
                //Alert.alert(res.message
            } else {
                this.setState({ isLoadingPurchase: false, IsPurchaseData: true, isFetchingPurchase: false })
                //Alert.alert(res.message)
            }

        }).catch(error => {
            console.log("get error:::: " + error.error)
            this.setState({ isLoadingPurchase: false, IsPurchaseData: true, isFetchingPurchase: false })

        });



    }


    async ForceUpdateCheck() {


        var data = new FormData();
        data.append('app_version', (Platform.OS == "ios") ? Utils.IosVersion : Utils.AndroidVerion);
        data.append('source', (Platform.OS == "ios") ? "2" : "1");


        CommonApiCall('Profile/forceUpdate', data).then(res => {


            if (res.code == 1) {

                if (res.data == 1) {
                    Alert.alert(
                        "The renewable Energy Hub",
                        "A new version of Renewable is available. Please update your app",
                        [

                            { text: "Update", onPress: () => Linking.openURL((Platform.OS == "ios") ? Utils.IosUrl : Utils.AndroidUrl) }
                        ],
                        { cancelable: false }
                    );
                }

                console.log("check ForceUpdateCheck api data >>>>>> " + res.message)

            } else {
                console.log("check ForceUpdateCheck api data >>>>>> " + res.message)
            }

        }).catch(error => {
            console.log("get error:::: " + error.error)


        });
    }



    async getDataPurchaseLoadMore(page) {

        this.setState({
            name: await retrieveData(AppConstant.NAME),
        })

        this.state.id = await retrieveData(AppConstant.ID)
        //this.state.token = await retrieveData(AppConstant.API_TOKEN)

        this.setState({
            token: await retrieveData(AppConstant.API_TOKEN),
        })

        console.log("get purchase page count >>>> " + page)


        var data = new FormData();
        data.append('per_page', "15");
        data.append('page', page);



        TokenCommonApiCall("Profile/leads_taken", data, this.state.token).then(res => {

            if (res.code == 1) {
                let listData = this.state.purchasedata;
                let data = listData.concat(res.data)
                this.setState({ isLoadingPurchase: true, purchasedata: data, IsPurchaseData: false })
                // console.log("get data ???>>>>> "+this.state.data)    
            } else {
                this.setState({ isLoadingPurchase: false })
                //Alert.alert(res.message)

                if (page == 1) {
                    this.setState({ IsPurchaseData: true })
                }
            }

        }).catch(error => {
            console.log("get error:::: " + error.error)
            this.setState({ isLoadingPurchase: false, IsPurchaseData: true })

        });



    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        console.log("check unmount");

        this.props.navigation.removeListener('blur', this._onBlur);
        this.props.navigation.removeListener('focus', this._onFocus);
    }

    UNSAFE_componentWillMount = () => {
        BackHandler.addEventListener('hardwareBackPress', () => { return false });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);


    }

    componentDidMount() {
        this.props.navigation.addListener('focus', this._onFocus);
        this.props.navigation.addListener('blur', this._onBlur);




    }
    renderFooterPurchase = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null

        if (!this.state.isLoadingPurchase) return null;

        return (
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingVertical: 10 }}>
                <ActivityIndicator animating size="large" color="#FFFFFF" />
            </View>
        );
    };

    handleLoadMorePurchase = () => {


        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                if (this.state.isLoadingPurchase) {
                    this.purchasepage = this.purchasepage + 1; // increase page by 1
                    //console.log("not search" + this.purchasepage);
                    this.getDataPurchaseLoadMore(this.purchasepage); // method for API call 
                }
            } else {
                Alert("No internet connection...");
            }

        });



    };


    renderFooter = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.isLoading) return null;

        return (
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingVertical: 10 }}>
                <ActivityIndicator animating size="large" color="#FFFFFF" />
            </View>
        );
    };

    handleLoadMore = () => {

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                if (this.state.isBuyAllpage) {
                    if (this.state.isLoading) {
                        this.buypage = this.buypage + 1; // increase page by 1
                        console.log("handleLoadMore buypage >>>>>> " + this.buypage);
                        this.getDataBuyLoadMore(this.buypage); // method for API call 
                    }
                }
            } else {
                Alert("No internet connection...");
            }

        });
    };

    Calls(item) {



        if (item.mobile == "") {
            Linking.openURL(`tel:${item.phone}`)
        } else {
            Linking.openURL(`tel:${item.mobile}`)
        }

    }


    render() {



        return (

            <ImageBackground source={require('../assets/home_bg.png')}
                style={styles.linearGradient}>

                <Spinner
                    visible={this.state.isMainLoading}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle} />


                <SafeAreaView style={styles.container}>
                    <View style={{ flexDirection: 'row', marginLeft: 12, marginRight: 12, marginTop: 15, }}>
                        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ justifyContent: 'center' }}>

                            <Image source={require('../assets/nav.png')} style={{ width: 30, height: 30, justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center' }} />
                        </TouchableOpacity>


                        <View style={{ flex: 1, marginRight: 10, }}>

                            <Image tintColor={color.color_white} source={require('../assets/logo.png')} style={{ width: 100, height: 61, tintColor: color.color_white, alignSelf: 'flex-end', }} />

                        </View>



                    </View>


                    <View style={{ display: 'none' }}>
                        <View style={{ marginTop: 10, marginLeft: 10, marginRight: 15, }}>

                            <Image source={require('../assets/new_profile.jpg')} style={{ height: 200, width: '100%', borderRadius: 15, }} />

                            <Text style={{ fontFamily: 'Poppins', fontWeight: '500', fontSize: 23, color: color.color_black, position: 'absolute', bottom: 0, alignSelf: 'center', marginBottom: 10, }}>Grow your business with{"\n"}The Renewable Energy Hub</Text>

                        </View>

                    </View>


                    <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, marginRight: 10, backgroundColor: color.color_light_gray, borderRadius: 10, }}>

                        <TouchableWithoutFeedback

                            onPress={() => this.setState({
                                buycolor: color.color_off_white,
                                purchasecolor: color.color_white,
                                IsBuy: false
                            })}>
                            <View style={{ borderTopLeftRadius: 10, borderBottomLeftRadius: 10, borderWidth: 2, borderColor: (this.state.IsBuy == true ? color.color_light_gray : color.color_white), flex: 1, padding: 10, justifyContent: 'center', alignSelf: 'center', alignItems: 'center', alignContent: 'center' }}>

                                <Text numberOfLines={1} style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center', alignContent: 'center', fontFamily: 'Poppins-Medium', fontSize: 16, textAlign: 'center', alignSelf: 'center', color: this.state.purchasecolor }} >Purchased Leads</Text>
                            </View>
                        </TouchableWithoutFeedback>



                        <TouchableWithoutFeedback

                            onPress={() => this.setState({
                                buycolor: color.color_white,
                                purchasecolor: color.color_off_white,
                                IsBuy: true
                            })}>

                            <View style={{ flex: 1, marginLeft: 5, justifyContent: 'center', alignSelf: 'center', alignItems: 'center', alignContent: 'center', borderTopRightRadius: 10, borderBottomRightRadius: 10, borderWidth: 2, borderColor: (this.state.IsBuy == true ? color.color_white : color.color_light_gray), padding: 10, }}>


                                <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 16, textAlign: 'center', alignSelf: 'center', color: this.state.buycolor }} >Buy NEW Leads</Text>
                            </View>
                        </TouchableWithoutFeedback>




                    </View>


                    <View style={{ display: (this.state.IsBuy == true ? 'none' : 'flex'), flex: 1 }}>



                        <View style={{ display: (this.state.IsPurchaseData == true ? 'flex' : 'none'), marginTop: 100, flex: 1, height: '100%', width: '100%', justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }}>
                            <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 20, color: color.color_white, justifyContent: 'center', alignSelf: 'center', flex: 1, textAlign: 'center', alignContent: 'center' }}>Sorry No purchase leads </Text>

                        </View>

                        <FlatList
                            data={this.state.purchasedata}
                            extraData={this.state}
                            onRefresh={() => this.onRefreshPurchas()}
                            refreshing={this.state.isFetchingPurchase}
                            renderItem={this.listItem}
                            keyExtractor={(item, index) => index.toString()}
                            style={{ display: (this.state.IsPurchaseData == false ? 'flex' : 'none'), flex: 1, marginLeft: 10, marginRight: 15, marginBottom: 20, marginTop: 10, }}
                            nestedScrollEnabled={true}
                            initialNumToRender={15} // Vary According to your screen size take a lumsum according to your item height
                            removeClippedSubviews={true}
                            ListFooterComponent={this.renderFooterPurchase.bind(this)}
                            onMomentumScrollEnd={this.handleLoadMorePurchase.bind(this)}
                            renderItem={({ item, index }) =>
                            (

                                <Card style={styles.cardviewpurchase}>
                                    <View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image source={require('../assets/location.png')} style={{ alignSelf: 'center', }} />
                                            <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 16, textAlign: 'center', marginLeft: 10, alignSelf: 'center', marginRight: 15, }} >{item.city}</Text>

                                        </View>

                                        <View style={{ borderStyle: 'dotted', borderWidth: 1, borderRadius: 1, marginTop: 10, borderColor: color.color_light_gray }} />

                                        <View style={{ marginTop: 10, flexDirection: 'row' }}>

                                            <Image source={require('../assets/location.png')} style={{ width: 15, height: 19, alignSelf: 'center' }} />
                                            <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray, marginRight: 15, }} >{item.type}</Text>
                                        </View>

                                        <View style={{ marginTop: 10, flexDirection: 'row', display: 'none' }}>

                                            <Image source={require('../assets/location.png')} style={{ width: 15, height: 19, alignSelf: 'center' }} />
                                            <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray, marginRight: 15, }} >{item.distance}</Text>
                                        </View>

                                        <View style={{ marginTop: 10, flexDirection: 'row' }}>

                                            <Image source={require('../assets/user_new.png')} style={{ marginLeft: -2, width: 20, height: 20, alignSelf: 'center' }} />
                                            <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray }} >{item.name}</Text>
                                        </View>

                                        <View style={{ marginTop: 10, flexDirection: 'row' }}>

                                            <Image source={require('../assets/rent.png')} style={{ marginLeft: -2, width: 20, height: 20, alignSelf: 'center' }} />
                                            <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray }} >{item.property}</Text>
                                        </View>

                                        <View style={{ flexDirection: 'row', marginTop: 10, }}>

                                            <View style={{ flex: 1, marginRight: 5, }}>
                                                <TouchableWithoutFeedback onPress={() => this.Calls(item)}>
                                                    <View style={{ borderColor: color.color_green, backgroundColor: color.color_green, borderRadius: 10, borderWidth: 1, paddingTop: 10, paddingBottom: 10, paddingLeft: 25, paddingRight: 25, }}>
                                                        <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', alignSelf: 'center', color: color.color_white }} >Call</Text>

                                                    </View>
                                                </TouchableWithoutFeedback>
                                            </View>


                                            <View style={{ flex: 1, marginLeft: 5 }}>
                                                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Purchasedetail', { lead_id: item.id })} >
                                                    <View style={{ borderColor: color.color_light_blue, backgroundColor: color.color_light_blue, borderRadius: 10, borderWidth: 1, padding: 10, }}>
                                                        <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', alignSelf: 'center', color: color.color_white }} >Client Details</Text>
                                                    </View>
                                                </TouchableWithoutFeedback>

                                            </View>


                                        </View>


                                    </View>


                                </Card>




                            )}
                        />


                    </View>

                    <View style={{ flex: 1, display: (this.state.IsBuy == true ? 'flex' : 'none') }}>

                        {/* <Text style={{color:color.color_white,fontSize:20,}}>Buy</Text> */}

                        <Text

                            style={{ includeFontPadding: false, width: '95%', marginTop: 20, fontFamily: "Poppins-Bold", fontSize: 18, color: color.color_white, padding: 10, textAlign: 'center', alignSelf: 'center', borderRadius: 10, borderWidth: 2, borderColor: color.color_white, }}>{"Available Credits: " + this.state.leadcredit} </Text>




                        <ScrollView
                            refreshControl={

                                <RefreshControl

                                    refreshing={this.state.isFetching_no}
                                    onRefresh={this.onRefresh_no.bind(this)}
                                />
                            }>


                            <View style={{ display: (this.state.IsBuyNoData == true ? 'flex' : 'none'), flex: 1, height: '100%', width: '100%', justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }}>
                                <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 20, color: color.color_white, justifyContent: 'center', alignSelf: 'center', flex: 1, textAlign: 'center', alignContent: 'center', marginTop: 100 }}>Sorry No new leads</Text>

                            </View>




                        </ScrollView>


                        <FlatList
                            data={this.state.buydata}
                            numColumns={2}
                            onRefresh={() => this.onRefresh()}
                            refreshing={this.state.isFetching}
                            renderItem={this.listItem}
                            initialNumToRender={15}
                            keyExtractor={(item, index) => item.id}
                            ListFooterComponent={this.renderFooter.bind(this)}
                            onMomentumScrollEnd={this.handleLoadMore.bind(this)}
                            style={{ display: (this.state.IsBuyNoData == false ? 'flex' : 'none'), marginLeft: 5, marginRight: 5, marginBottom: 20, marginTop: 10, }}
                            nestedScrollEnabled={true}
                            renderItem={({ item, index }) =>
                            (
                                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Other', { lead_id: item.id })}>
                                    <Card style={styles.cardview}>
                                        <View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={require('../assets/location.png')} style={{ alignSelf: 'center' }} />
                                                <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 16, textAlign: 'center', marginLeft: 10, alignSelf: 'center', marginRight: 15, }} >{item.city}</Text>

                                            </View>
                                            <View style={{ borderStyle: 'dotted', borderWidth: 1, borderRadius: 1, marginTop: 10, borderColor: color.color_light_gray }} />

                                            <View style={{ marginTop: 10, flexDirection: 'row' }}>

                                                <Image source={require('../assets/location.png')} style={{ width: 15, height: 19, alignSelf: 'center' }} />
                                                <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray, marginRight: 15, }} >{item.type}</Text>
                                            </View>

                                            <View style={{ marginTop: 10, flexDirection: 'row' }}>

                                                <Image source={require('../assets/location.png')} style={{ width: 15, height: 19, alignSelf: 'center' }} />
                                                <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray, marginRight: 15, }} >{item.distance}</Text>
                                            </View>



                                            <View style={{ marginTop: 10, flexDirection: 'row' }}>

                                                <Image source={require('../assets/cash.png')} style={{ marginLeft: -2, width: 20, height: 20, alignSelf: 'center' }} />
                                                <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray }} >{item.price}</Text>
                                            </View>

                                            <View style={{ marginTop: 10, flexDirection: 'row' }}>

                                                <Image source={require('../assets/cash.png')} style={{ marginLeft: -2, width: 20, height: 20, alignSelf: 'center' }} />
                                                <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textAlign: 'center', marginLeft: 10, alignSelf: 'center', color: color.color_normal_gray }} >{item.property}</Text>
                                            </View>

                                        </View>


                                    </Card>


                                </TouchableWithoutFeedback>
                            )} />



                    </View>


                </SafeAreaView>

            </ImageBackground>

        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        width: "100%",
        height: height,
    },
    cardview: {
        width: Dimensions.get('window').width / 2.18,
        backgroundColor: color.color_white,
        borderRadius: 10,
        elevation: 15,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        padding: 10,

    }, cardviewpurchase: {
        backgroundColor: color.color_white,
        borderRadius: 10,
        elevation: 15,
        marginTop: 10,
        padding: 10,

    }, spinnerTextStyle: {
        color: '#FFF'
    },
})

export default NewHome;