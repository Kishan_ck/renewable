import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View, Platform, Image, TouchableWithoutFeedback, Alert, BackHandler, Linking } from 'react-native';
import { color } from '../Common/Color';
import TextInput from '../Common/InputText';
import CommonText from "../Common/AllText";
import LinearGradient from 'react-native-linear-gradient'
import { HelperText } from 'react-native-paper';
import { Appbar, Badge, Card } from 'react-native-paper';
import { EmailValidate, EmptyString } from '../Common/ValidationUtil';
import NetInfo from "@react-native-community/netinfo";
import { CommonApiCall, TokenCommonApiCall } from "../Common/CommonApiCall";
import { storeData, retrieveData, storeJsonData, getJsonData } from '../Common/LocalDataStore';
import { AppConstant } from '../Common/AppConstant';
import Spinner from 'react-native-loading-spinner-overlay';
import { Utils } from '../Common/Utils';
import KeyboardManager from 'react-native-keyboard-manager';
import messaging from '@react-native-firebase/messaging';


if (Platform.OS == "ios") {
    KeyboardManager.setEnable(true);
    KeyboardManager.setEnableDebugging(false);
    KeyboardManager.setKeyboardDistanceFromTextField(10);
    KeyboardManager.setEnableAutoToolbar(true);
    KeyboardManager.setToolbarDoneBarButtonItemText("Done");
    KeyboardManager.setToolbarManageBehaviourBy("subviews"); // "subviews" | "tag" | "position"
    KeyboardManager.setToolbarPreviousNextButtonEnable(true);
    KeyboardManager.setToolbarTintColor('#0000FF'); // Only #000000 format is supported
    KeyboardManager.setToolbarBarTintColor('#FFFFFF'); // Only #000000 format is supported
    KeyboardManager.setShouldShowToolbarPlaceholder(true);
    KeyboardManager.setOverrideKeyboardAppearance(false);
    KeyboardManager.setKeyboardAppearance("default"); // "default" | "light" | "dark"
    KeyboardManager.setShouldResignOnTouchOutside(true);
    KeyboardManager.setShouldPlayInputClicks(true);
    KeyboardManager.resignFirstResponder();
    KeyboardManager.isKeyboardShowing()
}


class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: "",
            passvisible: true,
            emailerror: false,
            passerror: false,
            emailerrorlable: "",
            passerrorlable: "",
            passicon: require('../assets/eye.png'),
            isLoading: false,
        }


    }

    onButtonPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        // then navigate
        navigate('NewScreen');
    }

    handleBackButton = () => {
        Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )
        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        // this.removeId()
    }

    async removeId() {
        await storeData(AppConstant.ID, "");
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    async Validation() {

    
        
        const    authorizationStatus = await messaging().requestPermission(
                {
                  alert: true,
                  announcement: false,
                  badge: true,
                  carPlay: true,
                  provisional: false,
                  sound: true,
                }
              );
        
        

              console.log("check validation status ::::: "+ authorizationStatus )


       

        if (!EmptyString(this.state.email)) {
            this.setState({
                emailerror: true,
                emailerrorlable: "Enter Email/Username",

            });
            return false;

        }
        else if (!EmptyString(this.state.password)) {
            this.setState({
                emailerror: false,
                emailerrorlable: "",
                passerror: true,
                passerrorlable: "Enter password",

            });

            return false;
        }


        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);

           

            if(Platform.OS=="ios"){
                console.log("Utils FCMTOKEN LOGIN IOS : ", Utils.FCMTOKEN);
            

               

                  if (!authorizationStatus) {
                    Alert.alert(
                        "The Renewable Energy Hub",
                        "Please give your permission to get notifications of new leads. Select 'Go to Settings' below to enable this",
                        [
    
                            { text: "Settings", onPress: () => Linking.openURL('app-settings:') },
                            { text: "Cancel", onPress: () => 
                            {

                                if (state.isConnected) {
                                    this.onLogin(authorizationStatus);
                                } else {
                                    Alert("No internet connection...");
                                }

                            }
                        
                        }
                        ],
                        { cancelable: false }
                    );
                  }else{

                    if (state.isConnected) {
                        console.log("else condtion part")
                        this.onLogin(authorizationStatus);
                    } else {
                        Alert("No internet connection...");
                    }
                  }
            
            }else{
                console.log("Utils FCMTOKEN LOGIN ANDROID : ", Utils.FCMTOKEN);
            
            
                if (state.isConnected) {
                    this.onLogin();
                } else {
                    Alert("No internet connection...");
                }
            }

           // Utils.FCMTOKEN = ''


            



        });
    }

    async onLogin(authorizationStatus) {
        this.setState({
            passerror: false,
            passerrorlable: "",
            emailerror: false,
            emailerrorlable: "",
        });


        if(Platform.OS=="ios"){
            console.log("check status ::::: "+ authorizationStatus)
            if(authorizationStatus){
                const token = await messaging().getToken();

                Utils.FCMTOKEN = token;

                console.log("if condition >>> "+ token )
        
            }else{
                Utils.FCMTOKEN="f8dfGdNDmUHtnKojKsk4pT:APA91bGUfG2wX-6lpEBeoIUPDyyub6G5RfjMzbD8pM_dt8z8KQwXhNxD01yMDwWX--fZRnsBJ_wtJ68_-bd_5tQ4NrLyt7aVtBThFXdRYwjT8g1Q5g99HW3f2G74OjcUoePatDXklBaC"
           
           
                console.log("else condition >>> "+ Utils.FCMTOKEN )
            }
    
        }else{
            const token = await messaging().getToken();

            Utils.FCMTOKEN = token;
    
        }


        //Alert.alert(Utils.FCMTOKEN);
       
       

        //if(get_token!=null){
        this.setState({ isLoading: true })
        var data = new FormData();
        data.append('userName', this.state.email);
        data.append('password', this.state.password);
        data.append('deviceToken', Utils.FCMTOKEN);
        data.append('deviceType', (Platform.OS == "ios") ? "2" : "1");

        CommonApiCall('Login/login_post', data).then(res => {


            if (res.code == 1) {


                this.getData(res, res.data.id, res.token, res.data.name, res.data.leads_credits)

            } else {
                this.setState({ isLoading: false })
                Alert.alert(res.message)

            }

        }).catch(error => {
            console.log("get error:::: " + error.error)
            this.setState({ isLoading: false })

        });
        //}


    }


    async getData(res, id, token, name, leadscredits) {
        // await  AsyncStorage.setItem('id', response.data.data.id.toString());
        console.log("get tokennnnn >>>>>>>> " + token);

        console.log("iddddd >>>>>  " + id)
        this.setState({ isLoading: false })

        console.log("test1");

        Utils.NAME = name;

        await storeData(AppConstant.ID, id);
        await storeData(AppConstant.API_TOKEN, token);
        await storeData(AppConstant.NAME, name);
        await storeData(AppConstant.LEADSCREDITS, leadscredits);

        await storeJsonData("logindata", res);

        const logindata = await getJsonData("logindata");
        console.log("get local store login data >>>>>> " + logindata.data.id)

        Alert.alert(
            "The renewable Energy Hub",
            "Login Successful",
            [

                { text: "OK", onPress: () => this.props.navigation.navigate('DrawerNavigationRoutes') }
            ],
            { cancelable: false }
        );


    }



    passiconvisible() {
        if (this.state.passvisible) {
            this.setState({
                passvisible: false,
                passicon: require('../assets/invisible.png')
            })
        } else {
            this.setState({
                passvisible: true,
                passicon: require('../assets/eye.png')
            })
        }
    }


    render() {

        return (
            <LinearGradient
                colors={['#566C75', '#66859C', '#333540']}
                style={styles.linearGradient}>


                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>

                    <Spinner
                        visible={this.state.isLoading}
                        textContent={'Loading...'}
                        textStyle={styles.spinnerTextStyle}
                    />

                    <SafeAreaView style={styles.linearGradient}>



                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 56 }}>
                            <Image tintColor='#FFFFFF' source={require('../assets/logo.png')} style={{ tintColor: color.color_white, width: 132, height: 81 }} />
                        </View>


                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Card style={styles.cardmain}>

                                <View >
                                    <View style={styles.userviews}>
                                        <Image source={require('../assets/user.png')} style={styles.userimg} />
                                    </View>

                                    <CommonText
                                        text="Installer Login"
                                        fontFamily="Poppins-Bold"
                                        fontSize={24}
                                        color={color.color_gray}
                                        textstyles={styles.logintext} />

                                    <TextInput
                                        mode="outlined"
                                        label="Email/Username"
                                        type="email"
                                        value={this.state.email}
                                        onChangeText={text => this.setState({ email: text })}
                                        error={this.state.emailerror} />

                                    <HelperText type="error" visible={this.state.emailerror} style={[styles.errorlable, { display: (this.state.emailerror) ? 'flex' : 'none' }]}>
                                        {this.state.emailerrorlable}
                                    </HelperText>

                                    <TextInput
                                        mode="outlined"
                                        label="Password"
                                        value={this.state.password}
                                        onChangeText={text => this.setState({ password: text })}
                                        secureTextEntry={this.state.passvisible}
                                        iconpath={this.state.passicon}
                                        onPress={() => this.passiconvisible()}
                                        error={this.state.passerror}
                                        primarycolor={color.dark_green}
                                    />
                                    <HelperText type="error" visible={this.state.passerror} style={[styles.errorlable, { display: (this.state.passerror) ? 'flex' : 'none' }]}>
                                        {this.state.passerrorlable}
                                    </HelperText>


                                    <TouchableWithoutFeedback onPress={() => this.Validation()}>

                                        <Card style={styles.cardsignin}>

                                            <CommonText
                                                text="Sign in"
                                                fontFamily="Poppins-SemiBold"
                                                fontSize={16}
                                                color={color.color_white}
                                                textstyles={styles.signintext}
                                            />

                                        </Card>


                                    </TouchableWithoutFeedback>


                                </View>


                            </Card>


                        </View>



                    </SafeAreaView>

                </ScrollView>



            </LinearGradient>


        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    linearGradient: {

        flex: 1,
    },
    cardmain: {
        elevation: 15,
        marginLeft: 30,
        marginRight: 30,
        padding: 15,
        borderRadius: 10,
        justifyContent: 'center',
        marginTop: 50,
        marginBottom: 30,

    },
    logintext: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 50,

    },
    userviews: {
        borderRadius: 90,
        backgroundColor: color.color_gray,
        height: 90,
        width: 90,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        top: -60,
        position: 'absolute',
    },
    userimg: {
        width: 70,
        height: 70,
        alignSelf: 'center'
    },
    cardsignin: {
        backgroundColor: color.color_gray,
        borderRadius: 5,
        justifyContent: 'center',
        marginTop: 25,
        elevation: 10,
    },
    signintext: {
        padding: 15,
        alignSelf: 'center'
    }, spinnerTextStyle: {
        color: '#FFF'
    },
})

export default Login;