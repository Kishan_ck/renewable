import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View, Platform, Image, TouchableOpacity, ActivityIndicator, Alert, BackHandler, useWindow, Dimensions, Text, FlatList, ImageBackground, NativeModules, Modal } from 'react-native';
import { color } from '../Common/Color';
import { Appbar, Badge, Card } from 'react-native-paper';
import { storeData, retrieveData } from '../Common/LocalDataStore';
import { AppConstant } from '../Common/AppConstant';
import DropDownPicker from 'react-native-dropdown-picker';
import { WebView } from "react-native-webview";
import { Utils } from '../Common/Utils';
import Feather from "react-native-vector-icons/Feather";
import {  DrawerActions } from '@react-navigation/native';
Feather.loadFont();
class ByuNewCredits extends Component {

    constructor(props) {
        super(props)

        this.state = {
            country: '20',
            price: '£5',
            Alert_Visibility: false,
            isWeb: false,
            id: "",
            token: "",
            isLoading: false,
        }

    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        console.log("check unmount");

        this.props.navigation.removeListener('blur', this._onBlurtest);
        this.props.navigation.removeListener('focus', this._onFocustest);
    }

    componentDidMount() {
        console.log("check 1111111111");
        this.props.navigation.addListener('focus', this._onFocustest);
        this.props.navigation.addListener('blur', this._onBlurtest);


        const { navigation } = this.props;
        this.focusListener = navigation.addListener("didBlur", () => {
            // Call ur function here.. or add logic.     
            console.log("=====_onFocus1111111111")
        });


    }


    _onBlurtest = () => {

        console.log("=====_onBlur1111")

    };

    _onFocustest = () => {
        console.log("=====_onFocus11")

        this.setState({

            country: '20',
            price: '£5',
            Alert_Visibility: false,
            isWeb: false,
            isLoading: false,

        });



        this.getData()
    };


    async getData() {

        this.setState({

            token: await retrieveData(AppConstant.API_TOKEN),
            id: await retrieveData(AppConstant.ID)
        })

        console.log("get idddddd >>>>>>>  " + this.state.id)
    }

    async WorldPayPayment() {

        this.setState({ Alert_Visibility: false, isWeb: true, url: `${Utils.BASE_URL_PAYMENT}credit-purchase/${this.state.id}/${this.state.country}/worldpay` })

        console.log("check Word pay >>>>  " + `${Utils.BASE_URL_PAYMENT}credit-purchase/${this.state.id}/${this.state.country}/worldpay`)
        if (this.state.isWeb) {
            BackHandler.addEventListener('hardwareBackPress', () => { return true });

        } else {
            BackHandler.addEventListener('hardwareBackPress', () => { return false });

        }
    }

    async PaypalPayment() {

        this.setState({ Alert_Visibility: false, isWeb: true, url: `${Utils.BASE_URL_PAYMENT}credit-purchase/${this.state.id}/${this.state.country}/paypal` })

        console.log("check Word pay >>>>  " + `${Utils.BASE_URL_PAYMENT}api/credit-purchase/${this.state.id}/${this.state.country}/paypal`)

        BackHandler.addEventListener('hardwareBackPress', () => { return true });


    }

    handleWebViewNavigationStateChange = newNavState => {
        // newNavState looks something like this:
        // {
        //   url?: string;
        //   title?: string;
        //   loading?: boolean;
        //   canGoBack?: boolean;
        //   canGoForward?: boolean;
        // }
        const { url } = newNavState;


        if (url.includes('company-profile/credits')) {
            this.webview.stopLoading();
            Alert.alert("Payment Successful")
            this.setState({ isWeb: false })
            BackHandler.removeEventListener('hardwareBackPress', () => { return false });
            BackHandler.addEventListener('hardwareBackPress', () => { return false });

        }

        if (url.includes('dispatcher')) {
            this.setState({ isWeb: false })
            BackHandler.removeEventListener('hardwareBackPress', () => { return false });
            BackHandler.addEventListener('hardwareBackPress', () => { return false });

        }

        if (url.includes('paypal/cancel_return')) {
            this.setState({ isWeb: false })
            BackHandler.removeEventListener('hardwareBackPress', () => { return false });
            BackHandler.addEventListener('hardwareBackPress', () => { return false });

        }


    };

    render() {
        const SCRIPT = `
        const meta = document.createElement('meta');
        meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
        meta.setAttribute('name', 'viewport');
        document.head.appendChild(meta);`;

        return (
            <ImageBackground source={require('../assets/home_bg.png')}
                style={styles.linearGradient}>

                <View style={{ flex: 1, height: '100%', display: (this.state.isWeb == true ? 'flex' : 'none') }}>

                    <SafeAreaView style={{ flex: 1, height: "100%", width: '100%', resizeMode: 'cover' }}>

                        <WebView
                            ref={ref => (this.webview = ref)}
                            source={{ uri: this.state.url }}
                            javaScriptCanOpenWindowsAutomatically={true}
                            setSupportMultipleWindows={true}
                            domStorageEnabled={true}
                            decelerationRate="normal"
                            startInLoadingState={true}
                            automaticallyAdjustContentInsets={true}
                            androidHardwareAccelerationDisabled={true}
                            style={{ height: "100%", width: '100%', resizeMode: 'cover', flex: 1 }}
                            scalesPageToFit={true}
                            injectedJavaScript={SCRIPT}
                            onNavigationStateChange={this.handleWebViewNavigationStateChange} />

                    </SafeAreaView>


                </View>

                <SafeAreaView style={{ flex: 1, display: (this.state.isWeb == true ? 'none' : 'flex') }}>
                    <View style={{ flexDirection: 'row', marginLeft: 10, marginRight: 10, marginTop: 15, }}>
                        <TouchableOpacity onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())} style={{ justifyContent: 'center' }}>
                            <Image source={require('../assets/nav.png')} style={{ width: 30, height: 30, justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center' }} />
                        </TouchableOpacity>

                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 18, color: color.color_white, alignSelf: 'center' }}>Buy Credits</Text>
                        </View>

                        <View style={{ marginRight: 10, }}>
                            <Image tintColor={color.color_white} source={require('../assets/logo.png')} style={{ width: 100, height: 61, tintColor: color.color_white, alignSelf: 'flex-end', }} />
                        </View>
                    </View>

                    <ScrollView contentContainerStyle={{ flex: 1 }}>

                        <View style={{ flex: 1, marginTop: 30, }}>

                            <Card style={{ elevation: 20, backgroundColor: color.color_white, height: '80%', marginTop: 15, marginLeft: 10, marginRight: 20, flexDirection: 'column', justifyContent: 'center', alignContent: 'center' }}>

                                <View style={{ flex: 1, marginTop: 15, marginLeft: 10, marginRight: 10, flexDirection: 'column' }}>

                                    <Text
                                        style={{ fontFamily: "Poppins-Bold", fontSize: 20, color: color.color_black, padding: 10, alignSelf: 'center' }}>Buy Credits</Text>

                                    {
                                        (Platform.OS == "ios") ?

                                            <View style={{zIndex:20, marginTop: 15, width: '100%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>

                                                <View style={{ marginTop: 10, flex: 1, }}>
                                                    <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>Amount</Text>

                                                </View>

                                                <DropDownPicker



                                                     zIndex={10}

                                                    items={[
                                                        { label: '20', value: '20', },
                                                        { label: '30', value: '30', },
                                                        { label: '40', value: '40', },
                                                        { label: '60', value: '60', },
                                                        { label: '120', value: '120', },
                                                    ]}
                                                    placeholderStyle={{
                                                        fontWeight: 'bold',
                                                        textAlign: 'center'
                                                    }}
                                                    defaultValue={this.state.country}
                                                    containerStyle={{ flex: 1, height: 40 }}
                                                    style={{ backgroundColor: '#fafafa' }}

                                                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                                                    onChangeItem={item => this.setState({
                                                        country: item.value
                                                    })}
                                                />

                                            </View> :


                                            <View style={{ marginTop: 15, width: '100%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>

                                                <View style={{ marginTop: 10, flex: 1, }}>
                                                    <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>Amount</Text>

                                                </View>

                                                <DropDownPicker
                                                    items={[
                                                        { label: '20', value: '20', },
                                                        { label: '30', value: '30', },
                                                        { label: '40', value: '40', },
                                                        { label: '60', value: '60', },
                                                        { label: '120', value: '120', },
                                                    ]}
                                                    placeholderStyle={{
                                                        fontWeight: 'bold',
                                                        textAlign: 'center'
                                                    }}
                                                    defaultValue={this.state.country}
                                                    containerStyle={{ flex: 1, height: 40 }}
                                                    style={{ backgroundColor: '#fafafa' }}

                                                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                                                    onChangeItem={item => this.setState({
                                                        country: item.value
                                                    })}
                                                />

                                            </View>
                                    }



                                    <View style={{ marginTop: 25, width: '100%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>Price</Text>

                                        </View>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-Light', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>{this.state.price}</Text>

                                        </View>

                                    </View>

                                    <View style={{ marginTop: 15, width: '100%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>Sub Total</Text>

                                        </View>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-Light', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>{"£" + Number(this.state.country) * 5 + ".00"}</Text>

                                        </View>

                                    </View>

                                    <View style={{ marginTop: 15, width: '100%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>VAT</Text>

                                        </View>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-Light', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>{"£" + Number(this.state.country * 5 * 0.2) + ".00"}</Text>

                                        </View>

                                    </View>

                                    <View style={{ marginTop: 15, width: '100%', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>Grand Total</Text>

                                        </View>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontFamily: 'Poppins-Light', fontSize: 16, color: color.color_black, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>{"£" + Number(this.state.country * 5 * 1.2) + ".00"}</Text>

                                        </View>

                                    </View>

                                    <View style={{ marginTop: 10, flexDirection: 'column', alignSelf: 'center' }}>

                                        <View style={{ marginTop: 10, justifyContent: 'center', width: '70%' }}>
                                            <TouchableOpacity onPress={() => this.setState({ Alert_Visibility: true })} >
                                                <Card style={{ elevation: 10, borderRadius: 10, backgroundColor: color.color_orange, paddingLeft: 20, paddingRight: 20 }}>

                                                    <Text
                                                        style={{ fontFamily: "Poppins-Bold", fontSize: 16, color: color.color_white, padding: 10, alignSelf: 'center' }}>BUY NOW</Text>

                                                </Card>

                                            </TouchableOpacity>


                                        </View>
                                    </View>

                                </View>



                            </Card>


                            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", alignSelf: 'center' }}>

                                <Modal
                                    visible={this.state.Alert_Visibility}
                                    transparent={true}
                                    animationType={"fade"}
                                    onRequestClose={() => { this.cancelAlertBox(!this.state.Alert_Visibility) }} >

                                    <View style={{
                                        flex: 1,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        backgroundColor: "rgba(0, 0, 0, 0.2)", justifyContent: "center", alignItems: "center",
                                    }}>

                                        <View style={{ width: '70%', backgroundColor: color.color_white, padding: 20, borderRadius: 10, }}>

                                            <Text
                                                onPress={() => this.PaypalPayment()}
                                                style={{ fontFamily: "Poppins-Bold", fontSize: 14, color: color.color_white, padding: 10, alignSelf: 'center', backgroundColor: color.color_light_gray, width: '100%', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>Pay with PayPal</Text>

                                            <Text
                                                onPress={() => this.WorldPayPayment()}
                                                style={{ marginTop: 10, fontFamily: "Poppins-Bold", fontSize: 14, color: color.color_white, padding: 10, alignSelf: 'center', backgroundColor: color.color_light_gray, width: '100%', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>Pay with Credit / Debit Card</Text>

                                            <Text
                                                onPress={() => this.setState({ Alert_Visibility: false, })}
                                                style={{ marginTop: 10, fontFamily: "Poppins-Bold", fontSize: 14, color: color.color_white, padding: 10, alignSelf: 'center', backgroundColor: color.color_red, width: '100%', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>Cancel</Text>


                                        </View>

                                    </View>

                                </Modal>

                            </View>

                        </View>


                    </ScrollView>





                </SafeAreaView>

            </ImageBackground>
        )
    }
}

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        width: "100%",
        height: height,
    },
    cardview: {
        width: Dimensions.get('window').width / 2.18,
        backgroundColor: color.color_white,
        borderRadius: 10,
        elevation: 15,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        padding: 10,

    }, cardviewpurchase: {
        backgroundColor: color.color_white,
        borderRadius: 10,
        elevation: 15,
        marginTop: 10,
        padding: 10,

    }, spinnerTextStyle: {
        color: '#FFF'
    },
})



export default ByuNewCredits;

