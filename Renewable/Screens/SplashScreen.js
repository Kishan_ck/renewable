import React from 'react'
import { StyleSheet, View, SafeAreaView, Image, Platform,Alert,Linking } from 'react-native';

import messaging from '@react-native-firebase/messaging';
import { AppConstant } from '../Common/AppConstant';
import { storeData, retrieveData, removeAllData } from '../Common/LocalDataStore';

import NotificationController from "../NotificationController.android";
import NotificationControllerIOS from "../NotificationController.ios";
import { Utils } from '../Common/Utils';
import DeviceInfo from 'react-native-device-info';
import PushNotificationIos from '@react-native-community/push-notification-ios';

class SplashScreen extends React.Component {


  constructor(props) {
    super(props);

  }



  moveLR = () => {

    this.timeoutHandle = setTimeout(() => {
      // Add your logic for the transition
      this.getData()

    }, 5000);
  }


  componentDidMount = () => {
    this.moveLR();
    this.onAppBootstrap();
   
  }


  onAppBootstrap = async () => {

   
    // Register the device with FCM
    await messaging().registerDeviceForRemoteMessages();
    const name = await retrieveData(AppConstant.NAME);

    Utils.NAME = name;

    console.log("NAME : ", Utils.NAME);

    if (Platform.OS == "ios") {

      PushNotificationIos.getApplicationIconBadgeNumber((num)=>{ // get current number
    
          PushNotificationIos.setApplicationIconBadgeNumber(0) //set number to 0
       
    });

      const authorizationStatus = await messaging().requestPermission(
        {
          alert: true,
          announcement: false,
          badge: true,
          carPlay: true,
          provisional: false,
          sound: true,
        }
      );

      const token = await messaging().getToken();

      Utils.FCMTOKEN = token;

      await storeData(AppConstant.FCM_TOKEN, token);


      const get_token = await retrieveData(AppConstant.FCM_TOKEN);

      console.log("Utils FCMTOKEN IOS : ", Utils.FCMTOKEN);

      if (authorizationStatus) {
        console.log('Permission status:', authorizationStatus);
        Utils.FCMTOKEN = await messaging().getToken();

        await storeData(AppConstant.FCM_TOKEN, Utils.FCMTOKEN);
      }
      else{

        Alert.alert(
          "The renewable Energy Hub",
          "Your need to give a permission from notification settings. to get lead notifications",
          [
  
            { text: "Go to Setting", onPress: () => Linking.openURL('app-settings:') }
          ],
          { cancelable: false }
        );


        // let uniqueId = DeviceInfo.getUniqueId();

        // console.log("Test UDUD "+uniqueId);
  
        // Utils.FCMTOKEN = uniqueId;
      }

    } else {

      Utils.FCMTOKEN = await messaging().getToken();

      await storeData(AppConstant.FCM_TOKEN, Utils.FCMTOKEN);

      console.log("Utils FCMTOKEN ANDROID : ", Utils.FCMTOKEN);
    }

    // Save the token
    // await postToApi('/users/1234/tokens', { token });
  }
  async getData() {
    var id = await retrieveData(AppConstant.ID)
    //this.state.token = await retrieveData(AppConstant.API_TOKEN)

    console.log("check id get in store >>>>>  " + id);

    if (id != null) {
      this.props.navigation.navigate('DrawerNavigationRoutes');
    } else {
      this.props.navigation.navigate('Auth');
    }
  }

  render = () => {
    return (
      <SafeAreaView style={styles.mainContainer}>
        <Image source={require('../assets/logo.png')} style={styles.imglogo} />
        {
          (Platform.OS == "android") ? <NotificationController /> : <NotificationControllerIOS />
        }


      </SafeAreaView>
    )
  }

}
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  imglogo: {
    height: 150,
    width: 250,
  }

});
//   const Stack =createAppContainer( createStackNavigator ({
//     Home: {
//         screen: SplashScreen,
//         navigationOptions: {
//             headerShown: false,
//             gestureEnabled: false,
//         }
//     },

//    logn: {
//         screen: LoginScreen,
//         navigationOptions: {
//             headerShown: false
//         }
//     },

//   NavDrawer:{
//     screen:StackDrawerScreen,
//     navigationOptions: {
//       headerShown: false
//   }
//   },

//   Stacknavdrawer:{
//     screen:StackDrawerScreen,
//     navigationOptions: {
//       headerShown: false
//   }
//   }


// }))




export default SplashScreen;