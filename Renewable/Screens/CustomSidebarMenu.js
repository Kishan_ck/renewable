// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React from 'react';
import { View, Text, Alert, StyleSheet, SafeAreaView, Image } from 'react-native';

import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { storeData, retrieveData, removeAllData,NoretrieveData } from '../Common/LocalDataStore';
import { color } from '../Common/Color';
import { AppConstant } from '../Common/AppConstant';
import { Utils } from '../Common/Utils';
const CustomSidebarMenu = (props) => {

 
  console.log("check  CustomSidebarMenu "+Utils.NAME)

  
  return (


    <SafeAreaView style={{ flex: 1, backgroundColor: color.color_light_gray }}>
      <View style={{ flex: 1, flexDirection: 'column' }}>


        <View style={{ flexDirection: 'row', marginRight: 12, marginTop: 15, }}>
          <View style={{ flex: 1, flexDirection: 'column', marginLeft: 10, }}>
            <Text style={{ fontSize: 20, fontWeight: '600', fontFamily: 'Poppins-Medium', justifyContent: 'center', color: color.color_white }}>Hello,</Text>
            <Text style={{ fontSize: 20, fontWeight: '600', fontFamily: 'Poppins-Medium', justifyContent: 'center', color: color.color_white }}>{Utils.NAME}</Text>
          </View>




        </View>


        <SafeAreaView style={{ flex: 1, backgroundColor: 'transparent' }} >
          <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            <DrawerItem
              icon={() =>
                <View>
                  <Image source={require('../assets/logout.png')} style={{ height: 25, width: 25 }} />
                </View>
              }
              label={() => <Text style={{
                fontSize: 18,
                fontFamily: 'Poppins-SemiBold',
                left: 0,
                right: 0,
                color: color.color_white,

              }}>Logout</Text>}
              onPress={() => {
                props.navigation.toggleDrawer();
                Alert.alert(
                  'The renewable Energy Hub',
                  'Are you sure you want to logout?',
                  [
                    {
                      text: 'Cancel',
                      onPress: () => {
                        return null;
                      },
                    },
                    {
                      text: 'Ok',
                      onPress: () => {
                        removeAllData();
                        props.navigation.replace('Auth');
                      },
                    },
                  ],
                  { cancelable: false },
                );
              }}
            />
          </DrawerContentScrollView>
        </SafeAreaView>
      </View>
    </SafeAreaView>
  );
};

export default CustomSidebarMenu;

const stylesSidebar = StyleSheet.create({
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#307ecc',
    paddingTop: 40,
    color: 'white',
  },
  profileHeader: {
    flexDirection: 'row',
    backgroundColor: '#307ecc',
    padding: 15,
    textAlign: 'center',
  },
  profileHeaderPicCircle: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    color: 'white',
    backgroundColor: '#ffffff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileHeaderText: {
    color: 'white',
    alignSelf: 'center',
    paddingHorizontal: 10,
    fontWeight: 'bold',
  },
  profileHeaderLine: {
    height: 1,
    marginHorizontal: 20,
    backgroundColor: '#e2e2e2',
    marginTop: 15,
  },
});
