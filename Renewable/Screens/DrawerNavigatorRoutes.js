// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React
import React from 'react';

// Import Navigators from React Navigation
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { color } from '../Common/Color';
import { SafeAreaView, Dimensions, View, Image, Text, TouchableOpacity, Alert,BackHandler } from 'react-native';
import NewHomeScreens from "./NewHome.js";


import BuyNewCreditsScreen from "./BuyNewCredits";

import CustomSidebarMenu from './CustomSidebarMenu';


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const homeScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen
        name="HomeScreen"
        component={NewHomeScreens}
        options={{
          title: 'Home', //Set Header Title          
          headerShown: false
        }}

      />
     <Stack.Screen
        name="Buycreditscrns"
        component={BuyNewCreditsScreen}
        options={{
          headerShown: false
        }}

      />
      
    </Stack.Navigator>
  );
};

const settingScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="SettingsScreen"
    >
      <Stack.Screen
        name="SettingsScreen"
        component={BuyNewCreditsScreen}
        options={{
          title: 'Settings', //Set Header Title
          headerShown: false
        }}
      />
    </Stack.Navigator>
  );
};

const DrawerNavigatorRoutes = (props) => {
  return (
    <Drawer.Navigator
      backBehavior='initialRoute'
      drawerContentOptions={{
        activeTintColor: color.color_white,
        activeBackgroundColor: 'transparent',
        itemStyle: {
          marginTop: 10,
          marginRight: 20,
          borderBottomColor: color.color_white,
          borderBottomWidth: 1,
        },
        labelStyle: {
          color: 'white',
          fontSize: 18,
          fontFamily: 'Poppins-SemiBold',
        },
      }}
      screenOptions={{ headerShown: false }}
      drawerContent={CustomSidebarMenu}>
      <Drawer.Screen
        name="homeScreenStack"
        options={{
          drawerLabel: 'Home',
          drawerIcon: () => (
            <View>
              <Image source={require('../assets/home.png')} style={{ height: 25, width: 25 }} />
            </View>
          ),
        }}
        component={homeScreenStack}
      />
      <Drawer.Screen
        name="settingScreenStack"
        options={{
          drawerLabel: 'Buy Lead Credits',
          drawerIcon: () => (
            <View>
              <Image source={require('../assets/buy_credits.png')} style={{ height: 25, width: 25 }} />
            </View>
          ),
        }}
        component={settingScreenStack}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigatorRoutes;
